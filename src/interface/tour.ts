export interface ITour {
    name:string,
    location: string,
    description:string,
    date: string,
    tourOperator:string,
    price:number,
    img:string,
    id:string,
    type: string
  
}

export interface ITourClient {
    name:string,
    location: string,
    description:string,
    date: string,
    tourOperator:string,
    price:number,
    img:string,
    type: string
    }